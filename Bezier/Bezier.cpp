#include "Bezier.h"
#include <iostream>

using namespace std;
float bezier2(float t, float w[]) {
	return w[0] * (1 - t)*(1 - t) + 2 * w[1] * (1 - t)*t + w[2] * t * t;
}
float bezier3(float t, float w[]) {
	return (1 - t)*(1 - t)*(1 - t)*w[0] + 3 * w[1] * (1 - t)*(1 - t)*t + 3 * w[2] * (1 - t)*t*t + w[3] * t*t*t;
}
void DrawCurve2(SDL_Renderer *ren, Vector2D p1, Vector2D p2, Vector2D p3,SDL_Color fillColor)
{
	SDL_SetRenderDrawColor(ren, fillColor.r, fillColor.g, fillColor.b, fillColor.a);
	float w_x[] = { p1.x,p2.x,p3.x };
	float w_y[] = { p1.y,p2.y,p3.y };
	SDL_RenderDrawPoint(ren, p1.x, p1.y);
	int x_pre = p1.x;
	int y_pre = p1.y;
	for (int i = 1; i <= 100; i++)
	{
		float t = i * 1.0 / 100;
		float x = bezier2(t, w_x);
		float y = bezier2(t, w_y);
		SDL_RenderDrawLine(ren, x_pre, y_pre, (int)(x + 0.5), (int)(y + 0.5));
		x_pre = (int)(x + 0.5);
		y_pre = (int)(y + 0.5);

	}
	
	return;
}
void DrawCurve3(SDL_Renderer *ren, Vector2D p1, Vector2D p2, Vector2D p3, Vector2D p4,SDL_Color fillColor)
{
	SDL_SetRenderDrawColor(ren, fillColor.r, fillColor.g, fillColor.b, fillColor.a);
	float w_x[] = { p1.x,p2.x,p3.x,p4.x };
	float w_y[] = { p1.y,p2.y,p3.y,p4.y };
	SDL_RenderDrawPoint(ren, p1.x, p1.y);
	int x_pre = p1.x;
	int y_pre = p1.y;
	for (int i = 1; i <= 100; i++)
	{
		float t = i*1.0 / 100;
		float x = bezier3(t, w_x);
		float y = bezier3(t, w_y);
		SDL_RenderDrawLine(ren, x_pre, y_pre, (int)(x + 0.5), (int)(y + 0.5));
		x_pre = (int)(x + 0.5);
		y_pre = (int)(y + 0.5);
		
	}
	return;
}
