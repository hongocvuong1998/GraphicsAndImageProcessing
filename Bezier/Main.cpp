#include <iostream>
#include <SDL.h>
#include "Bezier.h"

using namespace std;

const int WIDTH = 800;
const int HEIGHT = 1000;

SDL_Event event;
void drawRect(SDL_Renderer* ren, SDL_Rect* rt, SDL_Color fillColor) {
	SDL_SetRenderDrawColor(ren, fillColor.r, fillColor.g, fillColor.b, fillColor.a);
	SDL_RenderDrawRect(ren, rt);
	return;
}
void clearWindows(SDL_Renderer* ren) {
	SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
	SDL_RenderClear(ren);
}
void changeMouse(Vector2D &p) {
	int x, y;
	while (event.type != SDL_MOUSEBUTTONUP) {
		SDL_GetMouseState(&x, &y);
		//cout << x << "---" << y << endl;
	}
	//cout << "mouse up" << endl;
	p.x = x;
	p.y = y;
	return;
}
bool isInsideRect(int x,int y,SDL_Rect *rt) {
	if (x >= rt->x && x <= rt->x + rt->w && y >= rt->y  && y <= rt->y + rt->h ) {
		//cout << "True" << endl;
		return true;
	}
	return false;
}
int main(int, char**) {
	//First we need to start up SDL, and make sure it went ok
	if (SDL_Init(SDL_INIT_VIDEO) != 0) {
		std::cout << "SDL_Init Error: " << SDL_GetError() << std::endl;
		return 1;
	}

	SDL_Window *win = SDL_CreateWindow("Hello World!", 0, 0, WIDTH, HEIGHT, SDL_WINDOW_SHOWN);
	//Make sure creating our window went ok
	if (win == NULL) {
		std::cout << "SDL_CreateWindow Error: " << SDL_GetError() << std::endl;
		return 1;
	}

	//Create a renderer that will draw to the window, -1 specifies that we want to load whichever
	//DON'T FORGET CHANGE THIS LINE IN YOUR SOURCE ----->>>>> SDL_RENDERER_SOFTWARE

	SDL_Renderer *ren = SDL_CreateRenderer(win, -1, SDL_RENDERER_SOFTWARE);
	if (ren == NULL) {
		SDL_DestroyWindow(win);
		std::cout << "SDL_CreateRenderer Error: " << SDL_GetError() << std::endl;
		SDL_Quit();
		return 1;
	}

	SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
	SDL_RenderClear(ren);
	SDL_SetRenderDrawColor(ren, 255, 0, 0, 255);
	//YOU CAN INSERT CODE FOR TESTING 
	Vector2D p1(22, 215), p2(94, 43), p3(140, 258), p4(213, 150);
	//Vector2D p1(120, 160), p2(35, 200), p3(220, 260), p4(220, 40);

	SDL_Rect *rect1 = new SDL_Rect();
	rect1->x = p1.x - 5;
	rect1->y = p1.y - 5;
	rect1->w = 10;
	rect1->h = 10;

	SDL_Rect *rect2 = new SDL_Rect();
	rect2->x = p2.x - 5;
	rect2->y = p2.y - 5;
	rect2->w = 10;
	rect2->h = 10;

	SDL_Rect *rect3 = new SDL_Rect();
	rect3->x = p3.x - 5;
	rect3->y = p3.y - 5;
	rect3->w = 10;
	rect3->h = 10;


	SDL_Rect *rect4 = new SDL_Rect();
	rect4->x = p4.x - 5;
	rect4->y = p4.y - 5;
	rect4->w = 10;
	rect4->h = 10;


	SDL_Color  colorRect = { 0, 255, 40, 255 };
	SDL_SetRenderDrawColor(ren, colorRect.r, colorRect.g, colorRect.b, colorRect.a);

	SDL_RenderDrawRect(ren, rect1);
	SDL_RenderDrawRect(ren, rect2);
	SDL_RenderDrawRect(ren, rect3);
	SDL_RenderDrawRect(ren, rect4);


	SDL_Color colorCurve4 = { 0, 255, 0, 255 };
	SDL_SetRenderDrawColor(ren, colorCurve4.r, colorCurve4.g, colorCurve4.b, colorCurve4.a);
	DrawCurve3(ren, p1, p2, p3, p4,colorCurve4);

    SDL_Color colorCurve3 = { 0, 0, 255, 255 };
	SDL_SetRenderDrawColor(ren, colorCurve3.r, colorCurve3.g, colorCurve3.b, colorCurve3.a);
	DrawCurve2(ren, p1, p2, p3,colorCurve3);
	
	
	SDL_RenderPresent(ren);
	

	//Take a quick break after all that hard work
	//Quit if happen QUIT event
	
	bool running = true;
	int _x, _y;
	bool _p1 = false, _p2 = false, _p3 = false, _p4 = false;
	while (running)
	{
		//cout << _x << "---" << _y << endl;
		//If there's events to handle
		if (SDL_PollEvent(&event))
		{
			
			//HANDLE MOUSE EVENTS!!!
			if (event.type == SDL_MOUSEBUTTONDOWN) {
			//	cout << "mouse down" << endl;
				SDL_GetMouseState(&_x, &_y);
				if (isInsideRect(_x, _y, rect1)) {
					_p1 = true;
					//cout << "p1 : " << _x << "---" << _y << endl;
				}
				else if (isInsideRect(_x, _y, rect2)) {
					_p2 = true;
					//cout << "p2 : " << _x << "---" << _y << endl;
				}
				else if (isInsideRect(_x, _y, rect3)) {
					_p3 = true;
					//cout << "p3 : " << _x << "---" << _y << endl;
				}
				else if (isInsideRect(_x, _y, rect4)) {
					_p4 = true;
					//cout << "p4 : " << _x << "---" << _y << endl;
				}
				else
					goto event_quit;
			}

			if (event.type == SDL_MOUSEBUTTONUP) {
				//cout << "mouse up" << endl;
				SDL_GetMouseState(&_x, &_y);
				if (_p1 == true) {
					p1.set(_x, _y);
					_p1 = false;
					//cout << "p1 : " << _x << "---" << _y << endl;
					rect1->x = p1.x - 5;
					rect1->y = p1.y - 5;
					rect1->w = 10;
					rect1->h = 10;
				}
				else if (_p2 == true) {
					p2.set(_x, _y);
					_p2 = false;
					//cout << "p2 : " << _x << "---" << _y << endl;
					rect2->x = p2.x - 5;
					rect2->y = p2.y - 5;
					rect2->w = 10;
					rect2->h = 10;
				}
				else if (_p3 == true) {
					p3.set(_x, _y);
					_p3 = false;
					//cout << "p3 : " << _x << "---" << _y << endl;
					rect3->x = p3.x - 5;
					rect3->y = p3.y - 5;
					rect3->w = 10;
					rect3->h = 10;
				}
				else if (_p4 == true) {
					p4.set(_x, _y);
					_p4 = false;
					rect4->x = p4.x - 5;
					rect4->y = p4.y - 5;
					rect4->w = 10;
					rect4->h = 10;
					//cout << "p4 : " << _x << "---" << _y << endl;
				}
				
				clearWindows(ren);
				
				drawRect(ren, rect1, colorRect);
				drawRect(ren, rect2, colorRect);
				drawRect(ren, rect3, colorRect);
				drawRect(ren, rect4, colorRect);

				DrawCurve2(ren, p1, p2, p3,colorCurve3);
				DrawCurve3(ren, p1, p2, p3, p4,colorCurve4);

				SDL_RenderPresent(ren);
			}

			SDL_GetMouseState(&_x, &_y);
			if (_p1 == true) {
				p1.set(_x, _y);
				rect1->x = p1.x - 5;
				rect1->y = p1.y - 5;
				rect1->w = 10;
				rect1->h = 10;
			}
			if (_p2 == true) {
				p2.set(_x, _y);
				rect2->x = p2.x - 5;
				rect2->y = p2.y - 5;
				rect2->w = 10;
				rect2->h = 10;

			}
			if (_p3 == true) {
				p3.set(_x, _y);
				rect3->x = p3.x - 5;
				rect3->y = p3.y - 5;
				rect3->w = 10;
				rect3->h = 10;

			}
			if (_p4 == true) {
				p4.set(_x, _y);
				rect4->x = p4.x - 5;
				rect4->y = p4.y - 5;
				rect4->w = 10;
				rect4->h = 10;

			}
			clearWindows(ren);

			drawRect(ren, rect1, colorRect);
			drawRect(ren, rect2, colorRect);
			drawRect(ren, rect3, colorRect);
			drawRect(ren, rect4, colorRect);

			DrawCurve2(ren, p1, p2, p3, colorCurve3);
			DrawCurve3(ren, p1, p2, p3, p4, colorCurve4);

			SDL_RenderPresent(ren);
		event_quit:
			//If the user has Xed out the window
			if (event.type == SDL_QUIT)
			{
				//Quit the program
				running = false;
			}
		}

	}
	
	SDL_DestroyRenderer(ren);
	SDL_DestroyWindow(win);
	SDL_Quit();

	return 0;
}
