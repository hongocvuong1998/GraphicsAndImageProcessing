#include "Ellipse.h"

void Draw4Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
	SDL_RenderDrawPoint(ren, x + xc, y + yc);
	SDL_RenderDrawPoint(ren, x + xc, -y + yc);
	SDL_RenderDrawPoint(ren, -x + xc, y + yc);
	SDL_RenderDrawPoint(ren, -x + xc, -y + yc);
}

void BresenhamDrawEllipse(int xc, int yc, int a, int b, SDL_Renderer *ren)
{

	int p = -2 * a*a*b + a*a + 2 * b*b;
	int x = 0;
	int y = b;
	SDL_RenderDrawPoint(ren, x + xc, y + yc);
	Draw4Points(xc, yc, x, y, ren);
	while (x*x*(a*a + b*b) < a*a*a*a)
	{
		if (p <= 0)
		{
			p += 4 * b*b*x + 6 * b*b;
		}
		else
		{
			y--;
			p += 4 * b*b*x - 4 * a*a*y + 4 * a*a + 6 * b*b;
		}
		x++;
		SDL_RenderDrawPoint(ren, x + xc, y + yc);
		Draw4Points(xc, yc, x, y, ren);
	}



	p = b*b + 2 * a*a - 2 * a*b*b;
	x = a;
	y = 0;
	SDL_RenderDrawPoint(ren, x + xc, y + yc);
	Draw4Points(xc, yc, x, y, ren);
	while (x*x*(a*a + b*b) > a*a*a*a)
	{
		if (p <= 0)
		{
			p += 4 * a*a*y + 6 * a*a;
		}
		else
		{
			x--;
			p += 4 * a*a*y + 6 * a*a - 4 * b*b*x + 4 * b*b;
		}
		y++;
		SDL_RenderDrawPoint(ren, x + xc, y + yc);
		Draw4Points(xc, yc, x, y, ren);
	}


}

void MidPointDrawEllipse(int xc, int yc, int a, int b, SDL_Renderer *ren)
{

    int  x = 0, y = b, p;
	p = b*b - a*a*b + a*a / 4;
	int B=b*b;
	int A=a*a;
	while (2.0*B*x <= 2.0*A*y)
	{
		if (p < 0)
		{
			p = p + 2*B*x + B;
			x++;
		}
		else
		{
			p = p + 2*B*x - 2 * B*y - B;
			x++; 
			y--;
		}
		Draw4Points(xc, yc, x, y, ren);
	}
	p = B*(x + 0.5)*(x + 0.5) + A*(y - 1)*(y - 1) - A*B;
	while (y > 0)
	{
		if (p <= 0)
		{
			p = p + 2 * B*x - 2 * A*y + A;
			x++; 
			y--;
		}
		else
		{
			p = p - 2*A*y + A;
			y--;
		}
		Draw4Points(xc, yc, x, y, ren);
	}


}
